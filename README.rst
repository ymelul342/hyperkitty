========================================
HyperKitty - Archiver for GNU Mailman v3
========================================

.. image:: https://gitlab.com/mailman/hyperkitty/badges/master/pipeline.svg?ignore_skipped=true&key_text=build&key_width=42
    :target: https://gitlab.com/mailman/hyperkitty/commits/master

.. image:: https://gitlab.com/mailman/hyperkitty/badges/master/coverage.svg?job=coverage
    :target: https://gitlab.com/mailman/hyperkitty/commits/master

.. image:: https://readthedocs.org/projects/hyperkitty/badge/?version=latest
    :target: https://hyperkitty.readthedocs.io

.. image:: http://img.shields.io/pypi/v/hyperkitty.svg
    :target: https://pypi.python.org/pypi/hyperkitty

.. image:: http://img.shields.io/pypi/dm/hyperkitty.svg
    :target: https://pypi.python.org/pypi/hyperkitty

HyperKitty is an open source Django application under development. It aims to
provide a web interface to access GNU Mailman v3 archives.

Why HyperKitty?
==============

HyperKitty is a replacement for Mailman's default Pipermail archiver. The default archiver is oveer 10 years old, and in those 10 years, users' expectations have changed and their more sophisticated requirements means that the current archiver can no longer deliver. Mailman3 is currently under development and it offers a pluggable architecture where multiple archivers can be plugged to the core without too much pain.

Some of the drawbacks of the current Pipermail Archiver include:

- It does not support stable URLS.
- It has scalability issues (it was not suitable for organizations working with hundred of thousand of messages per day, e.g, Launchpad)
- The web interface is outdated and does not output standards-compliant HTML nor does it take advantafe of new technologies (e.g, AJAX).

The HyperKitty archiver addresses most of the drawbacks of Pipermail and delivers a clean, fast, and painless experience.

Installation
============

Install The Code
----------------

Install the HyperKitty package and its dependencies with the following commands::

    sudo python setup.py install

You will also need to install the Sass CSS processor using your package manager or the project’s installation documentation. You can either use the dart-sass version (dartsass) or the C/C++ version, called libsass (the binary is sassc). The configuration file in example_project/settings.py defaults to the sassc version, but you just have to edit the COMPRESS_PRECOMPILERS mapping to switch to the dart-sass implementation, whose binary is called sass and which doesn’t recognize the short form of the -t/--style option.

We no longer recommend ruby-sass as there have been compatibility issues with recent versions.

Recent Debian and Ubuntu have a sassc package, which you can install with::

    sudo apt-get install sassc

It is however recommended to use Virtualenv to install HyperKitty, even for a non-development setup (production). Check out the development documentation for details.

Setting up the Databases
------------------------

The HyperKitty database is configured using the DATABASE setting in Django’s settings.py file, as usual. The database can be created with the following command::

    django-admin migrate --pythonpath example_project --settings settings

HyperKitty also uses a fulltext search engine. Thanks to the Django-Haystack library, the search engine backend is pluggable, refer to the Haystack documentation on how to install and configure the fulltext search engine backend.

HyperKitty’s default configuration uses the Whoosh backend, so if you want to use that you just need to install the Whoosh Python library.

Initial setup
-------------

After installing HyperKitty for the first time, you can populate the database with some data that may be useful, for example a set of thread categories to assign to your mailing-list threads. This can be done by running the following command::

    django-admin loaddata --pythonpath example_project --settings settings first_start

Thread categories can be edited and added from the Django administration interface (append /admin to your base URL).

You must also make sure that Mailman has generated the databases files that Postfix (or another MTA) will use to lookup the lists. Otherwise SMTP delivery will fail, and that will also impact HyperKitty when it will try to validate email addresses on registration. You can force Mailman to generate those database files with the following command::

    mailman aliases

Additional information about installation and a more in-depth beginner's guide is available at: https://docs.mailman3.org/projects/hyperkitty/en/latest/install.html


How To Contribute
=================

All code and non-code contributions are welcomed and appreciated.

To Contribute:
1. Clone repository onto local machine. Link for cloning can be found at the top right corner of the repository.
2. Make contribution on local machine.
3. Add changes and commit changes.
4. Initialize merge request with appropriate title and description explaining what changes were made and why.

Copyright & License
===================

Copyright (C) 2012-2019 by the Free Software Foundation, Inc.

HyperKitty is licensed under the `GPL v3.0 <http://www.gnu.org/licenses/gpl-3.0.html>`_

HyperKitty is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.

HyperKitty is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License along with HyperKitty. If not, see <http://www.gnu.org/licenses/>.


Help, Bugs, Feedback
====================

If you need help with HyperKitty, want to keep up with progress, chat with
developers, or ask any other questions about HyperKitty, you can hang out in the
IRC channel: `#mailman on irc.freenode.net <https://webchat.freenode.net/?channels=mailman>`_.
You can also subscribe to our `mailing list <https://lists.fedorahosted.org/admin/lists/hyperkitty-devel.lists.fedorahosted.org/>`_ of developers.

To report bugs, please create `a ticket here <https://gitlab.com/mailman/hyperkitty/issues>`_ or contact us on IRC.

About other links of feedback, please see below.

- User mailing list: https://lists.mailman3.org/archives/list/mailman-users@mailman3.org/
- The "mailman" developers links: https://mail.python.org/mailman/listinfo/mailman-developers/

Links
=====

- Full documentation: https://hyperkitty.readthedocs.org
- Project page and feedback: https://gitlab.com/mailman/hyperkitty
- Demo server: https://lists.mailman3.org/
- Local development setup: http://docs.mailman3.org/en/latest/devsetup.html
